"use strict";
const DATE_FORMAT = {
  weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'
};
const LATITUDE  = 43.26271;
const LONGITUDE = -2.92528;
const TIMEZONE  = "auto";

function Weathercode2Obj(n){
  let wmoObj ={
    code: n,
    weatherName: "",
    description: "",
    icon: ""
  }
  // We can use some symbols from here:
  // https://www.alt-codes.net/weather-symbols.php
  switch (n) {
    case 0:
      wmoObj.weatherName =  "CLEAR_SKY";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F323;";
      return wmoObj;
      break;
    case 1:
    case 2:
    case 3:
      wmoObj.weatherName =  "MAINLY_CLEAR";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F324;";
      return wmoObj;
      break;
    case 45:
    case 48:
      wmoObj.weatherName =  "FOG";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F32B;";
      return wmoObj;
      break;
    case 51:
    case 53:
    case 55:
      wmoObj.weatherName =  "DRIZZLE";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F326;";
      return wmoObj;
      break;
    case 56:
    case 57:
      wmoObj.weatherName =  "FREEZING_DRIZZLE";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F326;";
      return wmoObj;
      break;
    case 61:
    case 63:
    case 65:
      wmoObj.weatherName =  "RAIN";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F327;";
      return wmoObj;
      break;
    case 66:
    case 67:
      wmoObj.weatherName =  "FREEZING_RAIN";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F327;";
      return wmoObj;
      break;
    case 71:
    case 73:
    case 75:
      wmoObj.weatherName =  "SNOW_FALL";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F328;";
      return wmoObj;
      break;
    case 77:
      wmoObj.weatherName =  "SNOW_GRAINS";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F328;";
      return wmoObj;
      break;
    case 80:
    case 81:
    case 82:
      wmoObj.weatherName =  "RAIN_SHOWERS";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F327;";
      return wmoObj;
      break;
    case 85:
    case 86:
      wmoObj.weatherName =  "SNOW_SHOWERS";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F328;";
      return wmoObj;
      break;
    case 95:
      wmoObj.weatherName =  "THUNDERSTORM";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F329;";
      return wmoObj;
      break;
    case 96:
    case 99:
      wmoObj.weatherName =  "THUNDERSTORM_HAIL";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "&#x1F329";
      return wmoObj;
      break;

    default:
      wmoObj.weatherName =  "UNKNOWN_WEATHERCODE";
      wmoObj.description =  weather2description(wmoObj.weatherName);
      wmoObj.icon =         "?";
      return wmoObj;
      break;
  }
}

function appendDay(date, t_max, t_min, weather){
  let week_container = document.querySelector(".forecast");
  let day_element = new Day(date, t_max, t_min, weather);
  week_container.appendChild(day_element.createHTML());

}

function Day(date, t_max, t_min, weather){
  this.date = date;
  this.t_max = t_max;
  this.t_min = t_min;
  this.weatherName = weather.weatherName;
  this.weather_desc = weather.description;
  this.createHTML = function (){
    let day_element = document.createElement("article");
    day_element.setAttribute("class", "day-forecast");
    day_element.addEventListener("click",
      dayFetcher(this.date,
                (html)=>{
                  document.querySelectorAll(".hourly-forecast").forEach((x)=>x.remove());
                  day_element.appendChild(html);
                }));
    day_element.innerHTML = `
    <div class="day-forecast-text">
      <h2><time datetime="${this.date}">${(new Date(this.date)).toLocaleDateString(undefined, DATE_FORMAT)}</h2>
      <p>${this.weather_desc}</p>
      <ul>
          <li>&#x1F321;<sub>max</sub> ${this.t_max}&deg;C</li>
          <li>&#x1F321;<sub>min</sub> ${this.t_min}&deg;C</li>
      </ul>
    </div>
    <span class="day-forecast-image" title="${this.weather_desc}">${weather.icon}</span>`;
    return day_element;
  }
}

function weather2description(weather){
  let str = weather.replaceAll("_"," ").toLowerCase();
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function processDaily( response_obj ) {
  console.log(response_obj);
  let data = response_obj.daily;

  // Our variables are here:
  let time     = data.time;
  let temp_max = data.temperature_2m_max;
  let temp_min = data.temperature_2m_min;
  let code     = data.weathercode;

  for(let i = 0; i < time.length; i++){
    appendDay(time[i], temp_max[i], temp_min[i], Weathercode2Obj(code[i]));
  }
}

function dailyURL(){
  return `https://api.open-meteo.com/v1/forecast`+
    `?latitude=${LATITUDE}` +
    `&longitude=${LONGITUDE}` +
    `&timezone=${TIMEZONE}` +
    `&daily=temperature_2m_max,temperature_2m_min,weathercode`
}

fetch( dailyURL() )
  .then((r)=>r.json())
  .then(processDaily)
  .catch((err)=>console.error(err));


// Hourly data
function hourlyURL(date){
  return `https://api.open-meteo.com/v1/forecast`+
    `?latitude=${LATITUDE}` +
    `&longitude=${LONGITUDE}` +
    `&timezone=${TIMEZONE}` +
    `&hourly=temperature_2m,weathercode`+
    `&start_date=${date}`+
    `&end_date=${date}`;
}

function project(v, minv, maxv, min, max){
  let r = (v - minv)/(maxv - minv);
  return ((max-min)*r)+min;
}

function processHourly(data){
  let hourly_data = data.hourly;

  let temperature_unit = data.hourly_units.temperature_2m;

  let weathercode = hourly_data.weathercode;
  let temperature = hourly_data.temperature_2m;
  let time = hourly_data.time;

  let points = [];
  let t_min = Math.min.apply(null, temperature);
  let t_max = Math.max.apply(null, temperature);

  let xsize = 300;
  let xmargin = 15;
  let ysize = 90;
  let ymargin = 10;

  for( let i in temperature ){
    points.push( [project(i, 0, temperature.length, xmargin, xsize-xmargin),
                  project(temperature[i], t_min, t_max, ysize-ymargin, 0+20)] );
  }
  let pointstring = points.map((el)=>el[0].toString() + "," + el[1].toString() ).join(" ");
  let polyline = `<polyline class="graph-line" points="${pointstring}" fill="none" stroke="black" />`;

  let dots = points.map( (el)=> `
    <circle cx="${el[0]}" cy="${el[1]}" r="1.5" />
    `).join("");

  let labels = ""
  for( let i in points){
    labels += `
      <text x="${points[i][0]}" y="${points[i][1]-2}" class="temp-label">`+
      `${Math.round(temperature[i])}${temperature_unit}</text>`;
  }

  let icons = ""
  for( let i in points){
    labels += `<text x="${points[i][0]}" y="10" class="weather-label">` +
      `${Weathercode2Obj(weathercode[i]).icon}</text>`;
  }

  let hourly_element = document.createElement("article");
  hourly_element.setAttribute("class", "hourly-forecast");

  hourly_element.innerHTML = `
  <svg viewBox="0 0 ${xsize} ${ysize}" xmlns="http://www.w3.org/2000/svg">
    ${polyline}
    ${dots}
    ${labels}
    ${icons}
  </svg>
  `;
  return hourly_element;
}

function dayFetcher(date, insertionFunction){
  return (function () {
    fetch( hourlyURL(date) )
    .then((r)=>r.json())
    .then(processHourly)
    .then(insertionFunction)
    .catch((err)=>console.error(err));
  });
}
